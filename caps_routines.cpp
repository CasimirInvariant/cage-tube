#include "fullerene_caps.h"

void init_edgemarks()
{
  int i;
  EDGE *limit, *run;
  for (i = 0; (run = edge_array_start[i]) != 0; i++)
  {
    limit = run + how_many_edges[i];
    while (run != limit)
    {
      run->mark__ = 0;
      run++;
    }
  }
}
EmbeddableGraph write_planar_code_label_fix(int fix)
{
  static int first_call = 1;
  EDGE *temp, *remember, *checkstart, *start;
  static unsigned short next_number, actual_number, vertex, i;
  static unsigned short *code, *runcode, *runcode2;
  if (first_call)
  {
    first_call = 0;
    code = (unsigned short *)malloc(4 * maxlabel * sizeof(unsigned short));
    if (code == NULL)
    {
      fprintf(stderr, "Cannot allocate memory for the code\n");
      exit(0);
    }
  }
  runcode = code;
  RESETMARKS_V;
  for (i = 1; i <= fix; i++)
  {
    MARK_V(i);
    startedge_cgr[i] = firstedge[i];
    number_cgr[i] = i;
  }
  start = firstedge[1];
  if (fix == 0)
  {
    vertex = start->start;
    number_cgr[vertex] = 1;
    MARK_V(vertex);
    startedge_cgr[1] = start;
  }
  if (fix < 2)
  {
    if (start->end == outside)
    {
      checkstart = start;
      start = start->next;
      while ((start->end == outside) && (start != checkstart))
        start = start->next;
      if (start == checkstart)
      {
        Vertex v = {1, 0};
        vector<int> edgespace({1});
        vector<Vertex> vertices({v});
        return {edgespace, vertices};
      }
    }
    vertex = start->end;
    number_cgr[vertex] = 2;
    MARK_V(vertex);
    startedge_cgr[2] = start->invers;
  }
  actual_number = 1;
  if (fix >= 2)
    next_number = fix + 1;
  else
    next_number = 3;
  while (actual_number < next_number)
  {
    temp = remember = startedge_cgr[actual_number];
    do
    {
      vertex = temp->end;
      if (vertex != outside)
      {
        if (UNMARKED_V(vertex))
        {
          startedge_cgr[next_number] = temp->invers->next;
          number_cgr[vertex] = next_number;
          MARK_V(vertex);
          vertex = next_number;
          next_number++;
        }
        else
          vertex = number_cgr[vertex];
        (*runcode) = vertex;
        runcode++;
      }
      temp = temp->next;
    } while (temp != remember);
    (*runcode) = 0;
    runcode++;
    actual_number++;
  }
  actual_number--;
  
  vector<int> edgespace;
  vector<Vertex> vertices;
  Vertex v = {0, 0};
  int counts;

  for (runcode2 = code; runcode2 < runcode; runcode2++)
  {
    if (*runcode2 != 0)
    {
      edgespace.push_back(*runcode2);
      counts++;
    }
    else
    {
      v.valency = counts;
      counts = 0;
      vertices.push_back(v); // passing value instead of reference
      v.edgeind = v.edgeind + v.valency;
    }
  }
  return {edgespace, vertices};
}

int check_representation(EDGE *givenedge, VERTEX representation[], int clockwise)
{
  static EDGE *temp;
  static int next_number, actual_number, vertex;
  RESETMARKS_V;
  if (clockwise)
  {
    temp = givenedge->next;
    vertex = temp->start;
    number_cgr[vertex] = 0;
    MARK_V(vertex);
    vertex = temp->end;
    number_cgr[vertex] = 1;
    MARK_V(vertex);
    startedge_cgr[1] = temp->invers->next;
    temp = temp->next;
    vertex = temp->end;
    number_cgr[vertex] = 2;
    MARK_V(vertex);
    startedge_cgr[2] = temp->invers->next;
    actual_number = 1;
    next_number = 3;
    while (actual_number < next_number)
    {
      temp = startedge_cgr[actual_number];
      vertex = temp->end;
      if (vertex != outside)
      {
        if (UNMARKED_V(vertex))
        {
          startedge_cgr[next_number] = temp->invers->next;
          number_cgr[vertex] = next_number;
          MARK_V(vertex);
          vertex = next_number;
          next_number++;
        }
        else
          vertex = number_cgr[vertex];
      }
      if (vertex > (*representation))
        return (0);
      if (vertex < (*representation))
        return (2);
      representation++;
      temp = temp->next;
      vertex = temp->end;
      if (vertex != outside)
      {
        if (UNMARKED_V(vertex))
        {
          startedge_cgr[next_number] = temp->invers->next;
          number_cgr[vertex] = next_number;
          MARK_V(vertex);
          vertex = next_number;
          next_number++;
        }
        else
          vertex = number_cgr[vertex];
      }
      if (vertex > (*representation))
        return (0);
      if (vertex < (*representation))
        return (2);
      representation++;
      actual_number++;
    }
  }
  else
  {
    temp = givenedge->prev;
    vertex = temp->start;
    number_cgr[vertex] = 0;
    MARK_V(vertex);
    vertex = temp->end;
    number_cgr[vertex] = 1;
    MARK_V(vertex);
    startedge_cgr[1] = temp->invers->prev;
    temp = temp->prev;
    vertex = temp->end;
    number_cgr[vertex] = 2;
    MARK_V(vertex);
    startedge_cgr[2] = temp->invers->prev;
    actual_number = 1;
    next_number = 3;
    while (actual_number < next_number)
    {
      temp = startedge_cgr[actual_number];
      vertex = temp->end;
      if (vertex != outside)
      {
        if (UNMARKED_V(vertex))
        {
          startedge_cgr[next_number] = temp->invers->prev;
          number_cgr[vertex] = next_number;
          MARK_V(vertex);
          vertex = next_number;
          next_number++;
        }
        else
          vertex = number_cgr[vertex];
      }
      if (vertex > (*representation))
        return (0);
      if (vertex < (*representation))
        return (2);
      representation++;
      temp = temp->prev;
      vertex = temp->end;
      if (vertex != outside)
      {
        if (UNMARKED_V(vertex))
        {
          startedge_cgr[next_number] = temp->invers->prev;
          number_cgr[vertex] = next_number;
          MARK_V(vertex);
          vertex = next_number;
          next_number++;
        }
        else
          vertex = number_cgr[vertex];
      }
      if (vertex > (*representation))
        return (0);
      if (vertex < (*representation))
        return (2);
      representation++;
      actual_number++;
    }
  }
  return (1);
}
void get_representation(EDGE *givenedge, VERTEX representation[])
{
  EDGE *temp;
  static int next_number, actual_number, vertex;
  RESETMARKS_V;
  temp = givenedge->next;
  vertex = temp->start;
  number_cgr[vertex] = 0;
  MARK_V(vertex);
  vertex = temp->end;
  number_cgr[vertex] = 1;
  MARK_V(vertex);
  startedge_cgr[1] = temp->invers->next;
  temp = temp->next;
  vertex = temp->end;
  number_cgr[vertex] = 2;
  MARK_V(vertex);
  startedge_cgr[2] = temp->invers->next;
  actual_number = 1;
  next_number = 3;
  while (actual_number < next_number)
  {
    temp = startedge_cgr[actual_number];
    vertex = temp->end;
    if (vertex != outside)
    {
      if (UNMARKED_V(vertex))
      {
        startedge_cgr[next_number] = temp->invers->next;
        number_cgr[vertex] = next_number;
        MARK_V(vertex);
        vertex = next_number;
        next_number++;
      }
      else
        vertex = number_cgr[vertex];
    }
    (*representation) = vertex;
    representation++;
    temp = temp->next;
    vertex = temp->end;
    if (vertex != outside)
    {
      if (UNMARKED_V(vertex))
      {
        startedge_cgr[next_number] = temp->invers->next;
        number_cgr[vertex] = next_number;
        MARK_V(vertex);
        vertex = next_number;
        next_number++;
      }
      else
        vertex = number_cgr[vertex];
    }
    (*representation) = vertex;
    representation++;
    actual_number++;
  }
}
int l_patch_is_canonical(EDGE *mark, EDGE *starts[], int *numstarts,
                         EDGE *starts_mirror[], int *numstarts_mirror)
{
  int i, test;
  int auts = 1, mirror = 0;
  static VERTEX *representation = NULL;
  if (representation == NULL)
  {
    if ((representation = (VERTEX *)calloc(maxlabel * 2, sizeof(VERTEX))) == NULL)
    {
      fprintf(stderr, "Cannot allocate memory for the representation.\n");
      exit(0);
    }
  }
  get_representation(mark, representation);
  for (i = 0; i < *numstarts; i++)
    if (starts[i] != mark)
    {
      test = check_representation(starts[i], representation, 1);
      if (test == 2)
        return 0;
      if (test == 1)
        auts++;
    }
  for (i = 0; i < *numstarts_mirror; i++)
  {
    test = check_representation(starts_mirror[i], representation, 0);
    if (test == 2)
      return 0;
    if (test == 1)
    {
      mirror = 1;
      break;
    }
  }
  if (mirror)
    (auts_statistic_mirror[auts])++;
  else
    (auts_statistic[auts])++;
  if (mirror && -auts == symmetry || auts == symmetry || symmetry == 0)
  {
    return 1;
  }
  return 0;
}
int compute_sequence(EDGE *mark, int sequence[], EDGE *starts[], int *lengthp)
{
  EDGE *run;
  int i, j, length, counter, first;
  int buffer[13];
  run = mark;
  length = 0;
  do
  {
    length++;
    starts[length] = run;
    run = run->next->invers->next;
    counter = 0;
    while (run->end != outside)
    {
      counter++;
      run = run->invers->prev->invers->next;
    }
    sequence[length] = counter;
  } while (run != mark);
  sequence[0] = *lengthp = length;
  for (i = 1; i <= length; i++)
    buffer[i] = buffer[i + length] = sequence[i];
  for (i = 2, first = buffer[1]; i <= length; i++)
  {
    if (buffer[i] < first)
      return 0;
    if (buffer[i] == first)
    {
      for (j = 1; (j < length) && (buffer[i + j] == buffer[1 + j]); j++)
        ;
      if (j == length)
      {
        sequence[0] = i - 1;
        return 1;
      }
      if (buffer[i + j] < buffer[1 + j])
        return 0;
    }
  }
  return 1;
}
int compute_sequence_pentagon(EDGE *mark, EDGE *starts[], int *numstarts,
                              EDGE *starts_mirror[], int *numstarts_mirror)
{
  EDGE *run;
  int i, j, length, counter, first;
  int buffer[12];
  EDGE *edges[7];
  starts[0] = mark;
  *numstarts = 1;
  *numstarts_mirror = 0;
  run = mark = mark->next;
  length = 0;
  do
  {
    counter = 0;
    edges[length] = run->prev;
    do
    {
      counter++;
      run = run->invers->next->invers->prev;
    } while (!run->pentagon_right);
    buffer[length] = counter;
    length++;
  } while (run != mark);
  if (length == 1)
  {
    starts_mirror[0] = starts[0];
    *numstarts_mirror = 1;
    return 1;
  }
  edges[length] = edges[0];
  for (i = 0; i < length; i++)
    buffer[i + length] = buffer[i];
  for (i = 1, first = buffer[0]; i < length; i++)
  {
    if (buffer[i] < first)
      return 0;
    if (buffer[i] == first)
    {
      for (j = 1; (j < length) && (buffer[i + j] == buffer[j]); j++)
        ;
      if (j == length)
      {
        starts[*numstarts] = edges[i];
        (*numstarts)++;
      }
      else if ((j < length) && (buffer[i + j] < buffer[j]))
        return 0;
    }
  }
  for (i = 2 * length - 1; i >= length; i--)
  {
    if (buffer[i] == first)
    {
      for (j = 1; (j < length) && (buffer[i - j] == buffer[j]); j++)
        ;
      if (j == length)
      {
        starts_mirror[*numstarts_mirror] = edges[i - length + 1];
        (*numstarts_mirror)++;
      }
      else if ((j < length) && (buffer[i - j] < buffer[j]))
        return 0;
    }
  }
  return 1;
}
void init_new_paths(int maxnv_cap)
{
  int nv, length, i;
  EDGE *edges;
  edges = (EDGE *)malloc(3 * (1 + 2 + 3 + 4) * maxnv_cap * sizeof(EDGE));
  if (edges == NULL)
  {
    fprintf(stderr, "Can not get enough space for the cap edges \n");
    exit(1);
  }
  {
    int marki;
    EDGE *markrun;
    for (marki = 0; (marki < MAX_EDGE_ARRAYS) && (edge_array_start[marki] != 0); marki++)
      ;
    if (marki == MAX_EDGE_ARRAYS)
    {
      fprintf(stderr, "Constant MAX_EDGE_ARRAYS too small (1) -- increase.\n");
      exit(1);
    }
    edge_array_start[marki] = edges;
    how_many_edges[marki] = 3 * (1 + 2 + 3 + 4) * maxnv_cap;
    for (marki = 3 * (1 + 2 + 3 + 4) * maxnv_cap, markrun = edges; marki > 0; marki--, markrun++)
      markrun->mark__ = 0;
  }
  new__path = (EDGE **)malloc(sizeof(EDGE *) * (maxnv_cap + 1) * 4);
  if (new__path == NULL)
  {
    fprintf(stderr, "Can not get enough space for the new__path entries \n");
    exit(1);
  }
  new__path_last = (EDGE **)malloc(sizeof(EDGE *) * (maxnv_cap + 1) * 4);
  if (new__path_last == NULL)
  {
    fprintf(stderr, "Can not get enough space for the new__path_last entries \n");
    exit(1);
  }
  for (nv = 1; nv <= maxnv_cap; nv++)
    for (length = 1; length <= 4; length++, edges++)
    {
      new_path(nv, length) = edges;
      edges->start = nv;
      edges->pentagon_right = 0;
      edges->prev = edges + 2;
      edges->next = edges + 1;
      edges++;
      edges->start = nv;
      edges->end = outside;
      edges->pentagon_right = 0;
      edges->prev = edges - 1;
      edges->next = edges + 1;
      edges++;
      edges->start = nv;
      edges->pentagon_right = 0;
      edges->prev = edges - 1;
      edges->next = edges - 2;
      for (i = 1; i < length; i++)
      {
        edges++;
        edges->start = nv + i;
        edges->end = nv + (i - 1);
        edges->pentagon_right = 0;
        edges->prev = edges + 2;
        edges->next = edges + 1;
        edges->invers = edges - 1;
        (edges - 1)->invers = edges;
        (edges - 1)->end = nv + i;
        edges++;
        edges->start = nv + i;
        edges->end = outside;
        edges->pentagon_right = 0;
        edges->prev = edges - 1;
        edges->next = edges + 1;
        edges++;
        edges->start = nv + i;
        edges->pentagon_right = 0;
        edges->prev = edges - 1;
        edges->next = edges - 2;
      }
      new_path_last(nv, length) = edges;
    }
}
EDGE *make_n_gon(int n, int maxnv)
{
  static EDGE *edges = NULL;
  EDGE *run;
  int i;
  if (edges != NULL)
  {
    for (i = 0; edge_array_start[i] != edges; i++)
      ;
    for (; (i < MAX_EDGE_ARRAYS - 1) && (edge_array_start[i + 1] != 0); i++)
    {
      edge_array_start[i] = edge_array_start[i + 1];
      how_many_edges[i] = how_many_edges[i + 1];
    }
    edge_array_start[i] = 0;
    how_many_edges[i] = 0;
    free(edges);
  }
  edges = (EDGE *)malloc(sizeof(EDGE) * 3 * n);
  {
    int marki;
    EDGE *markrun;
    for (marki = 0; (marki < MAX_EDGE_ARRAYS) && (edge_array_start[marki] != 0); marki++)
      ;
    if (marki == MAX_EDGE_ARRAYS)
    {
      fprintf(stderr, "Constant MAX_EDGE_ARRAYS too small (2) -- increase.\n");
      exit(1);
    }
    edge_array_start[marki] = edges;
    how_many_edges[marki] = 3 * n;
    for (marki = 3 * n, markrun = edges; marki > 0; marki--, markrun++)
      markrun->mark__ = 0;
  }
  if (edges == NULL)
  {
    fprintf(stderr, "Do not get space for edges in make_n_gon.\n");
    exit(1);
  }
  if (firstedge == NULL)
  {
    if (n > maxnv)
    {
      fprintf(stderr, "Cannot construct n-gon for ");
      fprintf(stderr, "n>maxnv");
      exit(1);
    }
    firstedge = (EDGE **)malloc(sizeof(EDGE *) * (maxnv + 1));
    if (firstedge == NULL)
    {
      fprintf(stderr, "Do not get space for firstedge[].\n");
      exit(1);
    }
  }
  run = edges;
  if (n < 3)
  {
    fprintf(stderr, "The smallest possible face is a 3-gon.\n");
    exit(1);
  }
  firstedge[1] = run;
  run->start = 1;
  run->end = n;
  firstedge[1] = run;
  run->pentagon_right = 0;
  run->prev = run + 2;
  run->next = run + 1;
  run->invers = run + (3 * n - 1);
  run++;
  run->start = 1;
  run->end = outside;
  firstedge[0] = run;
  run->pentagon_right = 0;
  run->prev = run - 1;
  run->next = run + 1;
  run->invers = NULL;
  run++;
  run->start = 1;
  run->end = 2;
  run->pentagon_right = 0;
  run->prev = run - 1;
  run->next = run - 2;
  run->invers = run + 1;
  run++;
  for (i = 2; i < n; i++)
  {
    firstedge[i] = run;
    run->start = i;
    run->end = i - 1;
    firstedge[i] = run;
    run->pentagon_right = 0;
    run->prev = run + 2;
    run->next = run + 1;
    run->invers = run - 1;
    run++;
    run->start = i;
    run->end = outside;
    firstedge[i] = run;
    run->pentagon_right = 0;
    run->prev = run - 1;
    run->next = run + 1;
    run->invers = NULL;
    run++;
    run->start = i;
    run->end = i + 1;
    run->pentagon_right = 0;
    run->prev = run - 1;
    run->next = run - 2;
    run->invers = run + 1;
    run++;
  }
  firstedge[n] = run;
  run->start = n;
  run->end = n - 1;
  firstedge[n] = run;
  run->pentagon_right = 0;
  run->prev = run + 2;
  run->next = run + 1;
  run->invers = run - 1;
  run++;
  run->start = n;
  run->end = outside;
  firstedge[n] = run;
  run->pentagon_right = 0;
  run->prev = run - 1;
  run->next = run + 1;
  run->invers = NULL;
  run++;
  run->start = n;
  run->end = 1;
  run->pentagon_right = 0;
  run->prev = run - 1;
  run->next = run - 2;
  run->invers = edges;
  nv = n;
  if (n == 5)
  {
    pentagoncounter = 1;
    for (pentagonedges = 0; pentagonedges < 5; pentagonedges++)
    {
      pentagons[pentagonedges] = run;
      run->pentagon_right = 1;
      run = run->invers->prev;
    }
  }
  else
    pentagoncounter = pentagonedges = 0;
  return (edges + 1);
}
EDGE *add_n_gon(int n, EDGE *start)
{
  EDGE *run, *last;
  int length, i;
  if (construct_ipr && (n == 5))
  {
    last = start->next;
    if (last->pentagon_right)
      is_ipr = 0;
    last = last->invers->next;
    for (length = n - 2; last->end != outside; last = last->invers->next)
    {
      if (last->pentagon_right)
        is_ipr = 0;
      length--;
    }
  }
  else
  {
    last = start->next->invers->next;
    for (length = n - 2; last->end != outside; last = last->invers->next)
      length--;
  }
  if (length <= 0)
  {
    if (length < 0)
    {
      fprintf(stderr, "Error -- %d-gon does not fit in %d-hole \n", n, n - length);
      exit(4);
    }
    if (length == 0)
    {
      fprintf(stderr, "Didn't expect to add a polygon without a new vertex.\n");
      fprintf(stderr, "small changes necessary.\n");
      exit(5);
    }
  }
  run = new_path(nv + 1, length);
  start->invers = run;
  run->invers = start;
  start->end = nv + 1;
  run->end = start->start;
  for (i = 1; i <= length; i++, run = run->prev->invers)
    firstedge[nv + i] = run;
  run = new_path_last(nv + 1, length);
  last->invers = run;
  run->invers = last;
  last->end = nv + length;
  run->end = last->start;
  nv += length;
  if (n == 5)
  {
    for (; n; n--, start = start->invers->prev)
    {
      pentagons[pentagonedges] = start;
      pentagonedges++;
      start->pentagon_right = 1;
    }
    pentagoncounter++;
  }
  return (run->prev);
}
EDGE *delete_n_gon(EDGE *start)
{
  EDGE *run, *last;
  int length;
  if (start->next->pentagon_right)
  {
    for (run = start->next, length = 5; length; length--, run = run->invers->prev)
      run->pentagon_right = 0;
    pentagonedges -= 5;
    pentagoncounter--;
  }
  start->next->invers->end = outside;
  last = start->prev->invers->prev;
  for (length = 1; last->end == outside; last = last->prev->invers->prev)
    length++;
  last = last->next;
  last->end = outside;
  nv -= length;
  return (last);
}
EDGE *makepath(EDGE edgelist[])
{
  int i, vertices;
  EDGE *run;
  vertices = 2 * (l + m);
  if (m != 0)
  {
    run = edgelist;
    run->end = outside;
    run->invers = NULL;
    run++;
    run->invers = edgelist + 3 * vertices - 1;
    run->end = run->invers->start;
    run++;
    run->invers = run + 3;
    run->end = run->invers->start;
    run++;
    for (i = 1; i < l; i++)
    {
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run + 3;
      run->end = run->invers->start;
      run++;
      run->invers = run - 3;
      run->end = run->invers->start;
      run++;
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run - 3;
      run->end = run->invers->start;
      run++;
      run->invers = run + 3;
      run->end = run->invers->start;
      run++;
    }
    run->end = outside;
    run->invers = NULL;
    run++;
    run->invers = run + 4;
    run->end = run->invers->start;
    run++;
    run->invers = run - 3;
    run->end = run->invers->start;
    run++;
    if (m == 1)
    {
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run + 3;
      run->end = run->invers->start;
      run++;
      run->invers = run - 4;
      run->end = run->invers->start;
      run++;
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run - 3;
      run->end = run->invers->start;
      run++;
      run->invers = edgelist + 1;
      run->end = run->invers->start;
      run++;
    }
    else
    {
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run + 3;
      run->end = run->invers->start;
      run++;
      run->invers = run - 4;
      run->end = run->invers->start;
      run++;
      for (i = 1; i < m; i++)
      {
        run->end = outside;
        run->invers = NULL;
        run++;
        run->invers = run - 3;
        run->end = run->invers->start;
        run++;
        run->invers = run + 3;
        run->end = run->invers->start;
        run++;
        run->end = outside;
        run->invers = NULL;
        run++;
        run->invers = run + 3;
        run->end = run->invers->start;
        run++;
        run->invers = run - 3;
        run->end = run->invers->start;
        run++;
      }
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run - 3;
      run->end = run->invers->start;
      run++;
      run->invers = edgelist + 1;
      run->end = run->invers->start;
      run++;
    }
  }
  else
  {
    run = edgelist;
    run->end = outside;
    run->invers = NULL;
    run++;
    run->invers = run + 3 * (vertices - 1);
    run->end = run->invers->start;
    run++;
    run->invers = run + 3;
    run->end = run->invers->start;
    run++;
    for (i = 1; i < l; i++)
    {
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run + 3;
      run->end = run->invers->start;
      run++;
      run->invers = run - 3;
      run->end = run->invers->start;
      run++;
      run->end = outside;
      run->invers = NULL;
      run++;
      run->invers = run - 3;
      run->end = run->invers->start;
      run++;
      run->invers = run + 3;
      run->end = run->invers->start;
      run++;
    }
    run->end = outside;
    run->invers = NULL;
    run++;
    run->invers = edgelist + 1;
    run->end = run->invers->start;
    run++;
    run->invers = run - 3;
    run->end = run->invers->start;
    run++;
  }
  return edgelist;
}
EDGE *make_tube(int first_label, int rows)
{
  EDGE *edges, *run, *run2, *returnedge, *lastedge;
  int numvertices, i, j;
  if (l < 3)
  {
    fprintf(stderr, "Error -- value of l too small \n");
    exit(1);
  }
  if (rows == -1)
    return NULL;
  numvertices = 2 * (l + m) * (rows + 1);
  if ((edges = (EDGE *)malloc(sizeof(EDGE) * 3 * numvertices)) == NULL)
  {
    fprintf(stderr, "Do not get enough space for the edges of the tube.\n");
    exit(1);
  }
  {
    int marki;
    EDGE *markrun;
    for (marki = 0; (marki < MAX_EDGE_ARRAYS) && (edge_array_start[marki] != 0); marki++)
      ;
    if (marki == MAX_EDGE_ARRAYS)
    {
      fprintf(stderr, "Constant MAX_EDGE_ARRAYS too small (3) -- increase.\n");
      exit(1);
    }
    edge_array_start[marki] = edges;
    how_many_edges[marki] = 3 * numvertices;
    for (marki = 3 * numvertices, markrun = edges; marki > 0; marki--, markrun++)
      markrun->mark__ = 0;
  }
  for (i = 0, run = edges; i < numvertices; i++)
  {
    run->start = first_label + i;
    run->next = run + 2;
    run->prev = run + 1;
    run++;
    run->start = first_label + i;
    run->next = run - 1;
    run->prev = run + 1;
    run++;
    run->start = first_label + i;
    run->next = run - 1;
    run->prev = run - 2;
    run++;
  }
  returnedge = lastedge = makepath(edges);
  edges += 6 * (l + m);
  for (i = 1; i <= rows; i++, edges += 6 * (l + m))
  {
    run = lastedge->next->invers->prev;
    lastedge = run2 = makepath(edges);
    run->invers = run2;
    run->end = run2->start;
    run2->invers = run;
    run2->end = run->start;
    for (j = l + m - 1; j > 0; j--)
    {
      do
      {
        run = run->next;
        if (run->end != outside)
          run = run->invers;
      } while (run->end != outside);
      do
      {
        run2 = run2->prev;
        if (run2->end != outside)
          run2 = run2->invers;
      } while (run2->end != outside);
      run->invers = run2;
      run->end = run2->start;
      run2->invers = run;
      run2->end = run->start;
    }
  }
  if (m != 0)
  {
    for (i = l - 1; i; i--)
      returnedge = returnedge->next->invers->next->invers->next;
  }
  return returnedge;
}