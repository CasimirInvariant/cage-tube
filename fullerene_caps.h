#ifndef _FUL_CAP_H
#define _FUL_CAP_H

#include <cstdio>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include <climits>
#include <ctype.h>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

#define outside USHRT_MAX

typedef unsigned short VERTEX;
typedef unsigned short CODETYPE;

typedef struct e
{
	VERTEX start;
	VERTEX end;
	struct e *prev;
	struct e *next;
	struct e *invers;
        unsigned char pentagon_right;
	int dummy;
        unsigned short mark__;

} EDGE;

EDGE **firstedge=NULL;
int nv=0;
int l=0, m=0;

#define new_path(a,b) (new__path[((a)<<2)+((b)-1)])
#define new_path_last(a,b) (new__path_last[((a)<<2)+((b)-1)])

#define MARK_LIMIT (USHRT_MAX-3)

static unsigned short markvalue_v = MARK_LIMIT;
static unsigned short *marks__v;
#define RESETMARKS_V {int mki; if ((++markvalue_v) > MARK_LIMIT) \
       { markvalue_v = 1; for (mki=0;mki<=maxlabel;++mki) marks__v[mki]=0;}}
#define UNMARK_V(x) (marks__v[x] = 0)
#define ISMARKED_V(x) (marks__v[x] == markvalue_v)
#define UNMARKED_V(x) (marks__v[x] != markvalue_v)
#define MARK_V(x) (marks__v[x] = markvalue_v)

static unsigned short markvalue_e = MARK_LIMIT;
#define MAX_EDGE_ARRAYS 10
EDGE *edge_array_start[MAX_EDGE_ARRAYS];
int how_many_edges[MAX_EDGE_ARRAYS];
#define RESETMARKS_E {if ((++markvalue_e) > MARK_LIMIT) \
  { markvalue_e = 1; init_edgemarks();}}
#define UNMARK_E(x) ((x)->mark__ = 0)
#define ISMARKED_E(x) ((x)->mark__ == markvalue_e)
#define UNMARKED_E(x) ((x)->mark__ != markvalue_e)
#define MARK_E(x) ((x)->mark__ = markvalue_e)

EDGE **new__path;
EDGE **new__path_last;

EDGE *pentagons[30];
int pentagonedges=0;
int pentagoncounter=0;

int maxnv_cap=0;
int maxnv=0;
int maxlabel=0;

int construct_ipr=0;
int is_ipr=1;

int auts_statistic[7]={0}, auts_statistic_mirror[7]={0};

int symmetry = 0;

EDGE *isomorphism_tube;

static EDGE **startedge_cgr=NULL;
static VERTEX *number_cgr=NULL;

typedef struct Vertex
{
	int valency;
	int edgeind;
} Vertex;

typedef struct EmbeddableGraph
{
	vector<int> edgespace;
	vector<Vertex> vertices;
} EmbeddableGraph;

EDGE *add_n_gon(int, EDGE *);
int check_representation(EDGE *, VERTEX [], int);
int compute_sequence_pentagon(EDGE *, EDGE *[], int *, EDGE *[], int *);
int compute_sequence(EDGE *, int [], EDGE *[], int *);
EDGE *delete_n_gon(EDGE *);
void get_representation(EDGE *, VERTEX []);
void init_edgemarks();
void init_new_paths(int);
int l_patch_is_canonical(EDGE *, EDGE *[], int *, EDGE *[], int *);
EDGE *make_n_gon(int, int);
EDGE *make_tube(int, int);
EDGE *makepath(EDGE []);
EmbeddableGraph write_planar_code_label_fix(int);
int lm_patch_is_canonical(EDGE *);

class NanotubeEmbedder
{
	vector<vector<int>> *currentGraph;
    vector<vector<double>> *graphCoords;
    vector<vector<int>> *vertDepth;
    int nrVertices;
    int levelVertexCount = 0;
    int firstPentagon;
    int next = 1;
    const double bond_length = 1.42;
    const double vertexDistanceInHexagon = bond_length * 1.73205081;
    const double half_bond_length = bond_length * 0.5;
    const double epsilon = 1e-15;
    bool print;
    const double factors[3] = {0.05, 0.15, 0.25};
    int b1;
    int b2;

	public:
    NanotubeEmbedder();
    void embed(EmbeddableGraph);

	private:
	void writePDB(EmbeddableGraph);
	void calculateDepthsBFS();
	int findPlaceInNeighbours(int, int);
	int findFirstPentagon();
	void calculateEmbeddingForGraph();
	void findLocalOptimum(int, double, double, double, bool);
	double getDistance(int, int);
	void calculateBoundaryParameters();
	bool areNeighbours(int, int);
	bool haveCommonNeighbour(int, int);
	int getLengthBetween(int, int);
	void moveTowards(int, int, double);
	void moveTowardsEachother(int, int, double);
};

#endif