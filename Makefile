CFLAGS = -w -O4

TARG = tube$(SUFFIX)
OBJS = tube.o

all: $(TARG)

$(TARG): $(OBJS)
	$(CXX) $(CFLAGS) -o $(TARG) $(OBJS)

clean:
	rm -f *.o $(TARG)

