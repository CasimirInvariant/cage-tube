NanotubeEmbedder::NanotubeEmbedder()
{

}

void NanotubeEmbedder::embed(EmbeddableGraph eg)
{
    nrVertices = eg.vertices.size();
    currentGraph = new vector<vector<int>>(nrVertices, vector<int>(3));
    for (int j = 0; j < nrVertices; j++)
    {
        int st = eg.vertices[j].edgeind;
        int ed = eg.vertices[j].edgeind + eg.vertices[j].valency;
        for (int k = st; k < ed; k++)
        {
            (*currentGraph)[j][k - st] = eg.edgespace[k];
        }
    }
    graphCoords = new vector<vector<double>>(nrVertices, vector<double>(3));
    vertDepth = new vector<vector<int>>(2, vector<int>(nrVertices));
    calculateDepthsBFS();
    firstPentagon = findFirstPentagon();
    calculateBoundaryParameters();
    calculateEmbeddingForGraph();
    writePDB(eg);
    delete vertDepth;
    delete currentGraph;
    delete graphCoords;
}


void NanotubeEmbedder::writePDB(EmbeddableGraph eg)
{
    for (int i = 0; i < eg.vertices.size(); i++)
    {
        printf("ATOM  %5d  %3s              %8.3f%8.3f%8.3f                    %4s  \n", i + 1, "C", 
        (*graphCoords)[i][0], (*graphCoords)[i][1], (*graphCoords)[i][2], "C");
    }
    for (int i = 0; i < eg.vertices.size(); i++)
    {
        printf("CONECT%5d", i + 1);
        int st = eg.vertices[i].edgeind;
        int ed = eg.vertices[i].edgeind + eg.vertices[i].valency;
        for (int v = st; v < ed; v++)
        {
            printf("%5d", eg.edgespace[v]);
        }
    }
}

void NanotubeEmbedder::calculateDepthsBFS()
{
    int start = 0, current, previous, temp, index = 0;
    vector<int> seen(nrVertices);
    for (int i = 0; i < nrVertices; i++)
    {
        if ((*currentGraph)[i][2] == 0)
        {
            start = i;
            break;
        }
    }
    if ((*currentGraph)[(*currentGraph)[start][0] - 1][2] == 0)
    {
        current = (*currentGraph)[start][0] - 1;
        previous = start;
        if ((*currentGraph)[(*currentGraph)[start][1] - 1][0] == start + 1 && (*currentGraph)[(*currentGraph)[(*currentGraph)[start][1] - 1][1] - 1][2] == 0)
            next = -1;
        else if ((*currentGraph)[(*currentGraph)[start][1] - 1][1] == start + 1 && (*currentGraph)[(*currentGraph)[(*currentGraph)[start][1] - 1][2] - 1][2] == 0)
            next = -1;
        else if ((*currentGraph)[(*currentGraph)[(*currentGraph)[start][1] - 1][0] - 1][2] == 0)
            next = -1;
    }
    else
    {
        previous = (*currentGraph)[start][0] - 1;
        if ((*currentGraph)[(*currentGraph)[start][0] - 1][0] == start + 1)
        {
            if ((*currentGraph)[(*currentGraph)[(*currentGraph)[start][0] - 1][2] - 1][2] == 0)
            {
                next = -1;
                current = (*currentGraph)[(*currentGraph)[start][0] - 1][2] - 1;
            }
            else
                current = (*currentGraph)[(*currentGraph)[start][0] - 1][1] - 1;
        }
        else if ((*currentGraph)[(*currentGraph)[start][0] - 1][1] == start + 1)
        {
            if ((*currentGraph)[(*currentGraph)[(*currentGraph)[start][0] - 1][0] - 1][2] == 0)
            {
                next = -1;
                current = (*currentGraph)[(*currentGraph)[start][0] - 1][0] - 1;
            }
            else
                current = (*currentGraph)[(*currentGraph)[start][0] - 1][2] - 1;
        }
        else if ((*currentGraph)[(*currentGraph)[(*currentGraph)[start][0] - 1][1] - 1][2] == 0)
        {
            next = -1;
            current = (*currentGraph)[(*currentGraph)[start][0] - 1][1] - 1;
        }
        else
            current = (*currentGraph)[(*currentGraph)[start][0] - 1][0] - 1;
    }
    (*vertDepth)[0][index] = current;
    seen[current] = 1;
    (*vertDepth)[1][index++] = 0;
    while (current != start)
    {
        temp = findPlaceInNeighbours(current, previous + 1) + next + 3;
        temp %= 3;
        previous = current;
        current = (*currentGraph)[current][temp] == 0 ? (next == 1 ? (*currentGraph)[current][0] - 1 : (*currentGraph)[current][1] - 1) : (*currentGraph)[current][temp] - 1;
        while ((*currentGraph)[current][2] != 0)
        {
            temp = findPlaceInNeighbours(current, previous + 1) + next + 3;
            temp %= 3;
            previous = current;
            current = (*currentGraph)[current][temp] - 1;
        }
        (*vertDepth)[0][index] = current;
        seen[current] = 1;
        (*vertDepth)[1][index++] = 0;
    }
    start = 0;
    int buffer = -1;
    while (start < nrVertices)
    {
        for (int i = 2; i >= 0; i--)
        {
            if ((*currentGraph)[(*vertDepth)[0][start]][i] != 0)
            {
                current = (*currentGraph)[(*vertDepth)[0][start]][i] - 1;
                if (seen[current] == 0)
                {
                    if (buffer != -1 && getLengthBetween(current, (*vertDepth)[0][index - 1]) < 3)
                    {
                        (*vertDepth)[0][index] = buffer;
                        (*vertDepth)[1][index++] = (*vertDepth)[1][start] + 1;
                        seen[buffer] = 1;
                        buffer = -1;
                    }
                    if ((*vertDepth)[1][start] + 1 == (*vertDepth)[1][index - 1] && getLengthBetween(current, (*vertDepth)[0][index - 1]) > 2)
                    {
                        if (buffer != -1)
                        {
                            temp = (*vertDepth)[0][index - 2];
                            (*vertDepth)[0][index - 2] = (*vertDepth)[0][index - 1];
                            (*vertDepth)[0][index - 1] = temp;
                            (*vertDepth)[0][index] = buffer;
                            (*vertDepth)[1][index++] = (*vertDepth)[1][start] + 1;
                            seen[buffer] = 1;
                            buffer = -1;
                            i += 1;
                        }
                        else
                            buffer = current;
                    }
                    else
                    {
                        (*vertDepth)[0][index] = current;
                        (*vertDepth)[1][index++] = (*vertDepth)[1][start] + 1;
                        seen[current] = 1;
                    }
                }
            }
        }
        start++;
    }
}

int NanotubeEmbedder::findPlaceInNeighbours(int vertex, int neighbour)
{
    for (int i = 0; i < 3; i++)
    {
        if ((*currentGraph)[vertex][i] == neighbour)
            return i;
    }
    return -1;
}
int NanotubeEmbedder::findFirstPentagon()
{
    int t1 = -1, t2 = 0, count;
    for (int i = 0; i < nrVertices; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            t2 = (*vertDepth)[0][i];
            if ((*currentGraph)[t2][j] == 0)
                break;
            t1 = (*currentGraph)[t2][j] - 1;
            count = 1;
            do
            {
                for (int k = 0; k < 3; k++)
                {
                    if ((*currentGraph)[t1][k] == t2 + 1)
                    {
                        t2 = t1;
                        t1 = (*currentGraph)[t2][(k + 2) % 3] - 1;
                        if (t1 == -1)
                            t1 = (*currentGraph)[t2][1] - 1;
                        break;
                    }
                }
                count++;
            } while (t1 != (*vertDepth)[0][i] && count < 6);
            if (count == 5)
                return i;
        }
    }
    return -1;
}
void NanotubeEmbedder::calculateEmbeddingForGraph()
{
    int temp, start = 0, offset = 0, i = 0, begin = 0;
    double prevX = -vertexDistanceInHexagon, prevZ = 0, beginX = 0, beginZ = 0;
    bool horizontal = true;
    while ((*vertDepth)[1][i] == 0)
    {
        temp = (i + 1 >= nrVertices || (*vertDepth)[1][i + 1] != 0) ? (*vertDepth)[0][0] : (*vertDepth)[0][i + 1];
        if (areNeighbours((*vertDepth)[0][i], temp + 1))
            start = i;
        i++;
        levelVertexCount = i;
    }
    i = 0;
    while ((*vertDepth)[1][i + 1] == 0)
    {
        if (areNeighbours((*vertDepth)[0][i], (*vertDepth)[0][i + 1] + 1))
        {
            horizontal = true;
            beginX = -vertexDistanceInHexagon;
            beginZ = 0;
            break;
        }
        else if (!haveCommonNeighbour((*vertDepth)[0][i], (*vertDepth)[0][i + 1]))
        {
            horizontal = false;
            beginX = -vertexDistanceInHexagon / 2;
            beginZ = bond_length + half_bond_length;
            break;
        }
        i++;
    }
    i = 0;
    if (areNeighbours((*vertDepth)[0][0], (*vertDepth)[0][levelVertexCount - 1] + 1))
    {
        i++;
        offset = -1;
        begin = 1;
    }
    if (!haveCommonNeighbour((*vertDepth)[0][0], (*vertDepth)[0][levelVertexCount - 1] + 1))
    {
        beginX = -vertexDistanceInHexagon;
        beginZ = bond_length;
    }
    while ((*vertDepth)[1][i] == 0)
    {
        temp = (i + 1 >= nrVertices || (*vertDepth)[1][i + 1] != 0) ? (*vertDepth)[0][0] : (*vertDepth)[0][i + 1];
        if (horizontal)
        {
            prevX += vertexDistanceInHexagon;
        }
        else
        {
            prevX += vertexDistanceInHexagon / 2;
            prevZ -= bond_length + half_bond_length;
        }
        (*graphCoords)[(*vertDepth)[0][i]][0] = prevX;
        (*graphCoords)[(*vertDepth)[0][i]][2] = prevZ;
        (*graphCoords)[(*vertDepth)[0][i + levelVertexCount + offset]][0] = prevX - vertexDistanceInHexagon / 2;
        (*graphCoords)[(*vertDepth)[0][i + levelVertexCount + offset]][2] = prevZ - (horizontal ? half_bond_length : -half_bond_length);
        if (areNeighbours((*vertDepth)[0][i], temp + 1))
        {
            horizontal = false;
            prevZ -= half_bond_length;
            prevX += vertexDistanceInHexagon / 2;
            (*graphCoords)[temp][0] = prevX;
            (*graphCoords)[temp][2] = prevZ;
            i++;
            offset -= 1;
        }
        else if (!haveCommonNeighbour((*vertDepth)[0][i], temp))
        {
            horizontal = true;
            prevZ -= bond_length;
            offset += 1;
            (*graphCoords)[(*vertDepth)[0][i + levelVertexCount + offset]][0] = prevX;
            (*graphCoords)[(*vertDepth)[0][i + levelVertexCount + offset]][2] = prevZ;
        }
        i++;
    }
    temp = 0;
    for (i = 2; i <= (*vertDepth)[1][firstPentagon]; i += 2)
    {
        for (int j = 0; j < levelVertexCount; j++)
        {
            temp = i * levelVertexCount + j;
            (*graphCoords)[(*vertDepth)[0][temp]][0] = (*graphCoords)[(*vertDepth)[0][temp - 2 * levelVertexCount]][0] - vertexDistanceInHexagon / 2;
            (*graphCoords)[(*vertDepth)[0][temp]][2] = (*graphCoords)[(*vertDepth)[0][temp - 2 * levelVertexCount]][2] - bond_length - half_bond_length;
            (*graphCoords)[(*vertDepth)[0][temp + levelVertexCount]][0] = (*graphCoords)[(*vertDepth)[0][temp - levelVertexCount]][0] - vertexDistanceInHexagon / 2;
            (*graphCoords)[(*vertDepth)[0][temp + levelVertexCount]][2] = (*graphCoords)[(*vertDepth)[0][temp - levelVertexCount]][2] - bond_length - half_bond_length;
        }
    }
    i = 0;
    while ((*vertDepth)[1][i] <= (*vertDepth)[1][firstPentagon] + 1)
        i++;
    vector<int> amount((*vertDepth)[1][nrVertices - 1] + 1);
    while (i < nrVertices)
    {
        amount[(*vertDepth)[1][i]]++;
        i++;
    }
    double distance = sqrt(pow(b2 * vertexDistanceInHexagon / 2 + b1 * vertexDistanceInHexagon, 2) + pow(b2 * (half_bond_length + bond_length), 2));
    double distOnX = b2 * vertexDistanceInHexagon / 2 + b1 * vertexDistanceInHexagon;
    double angle = acos(distOnX / distance);
    double aCos = cos(angle);
    double aSin = sin(angle);
    double xPrev;
    for (i = 0; i < nrVertices; i++)
    {
        temp = (*vertDepth)[0][i];
        xPrev = (*graphCoords)[temp][0];
        (*graphCoords)[temp][0] = xPrev * aCos - (*graphCoords)[temp][2] * aSin;
        (*graphCoords)[temp][2] = xPrev * aSin + (*graphCoords)[temp][2] * aCos;
    }
    i = 0;
    while ((*vertDepth)[1][i] <= (*vertDepth)[1][firstPentagon])
        i++;
    prevZ = (*graphCoords)[(*vertDepth)[0][i]][2];
    while ((*vertDepth)[1][i] <= (*vertDepth)[1][firstPentagon] + 1)
    {
        if ((*graphCoords)[(*vertDepth)[0][i]][2] < prevZ)
            prevZ = (*graphCoords)[(*vertDepth)[0][i]][2];
        i++;
    }
    beginX = (*graphCoords)[(*vertDepth)[0][2 * levelVertexCount]][0] - (*graphCoords)[(*vertDepth)[0][0]][0];
    while (i < nrVertices)
    {
        start = i;
        distOnX = distance / amount[(*vertDepth)[1][start]];
        prevZ -= bond_length;
        prevX = beginX * ((*vertDepth)[1][start] / 2);
        while (i < nrVertices && (*vertDepth)[1][start] == (*vertDepth)[1][i])
        {
            (*graphCoords)[(*vertDepth)[0][i]][0] = distOnX * (i - start) + prevX;
            (*graphCoords)[(*vertDepth)[0][i]][2] = prevZ;
            i++;
        }
    }
    i = 0;
    double radius = distance / (2.0 * M_PI);
    beginX = distance;
    while (i < nrVertices)
    {
        angle = (*graphCoords)[(*vertDepth)[0][i]][0] * M_PI * 2 / distance;
        (*graphCoords)[(*vertDepth)[0][i]][0] = radius * cos(angle);
        (*graphCoords)[(*vertDepth)[0][i]][1] = radius * sin(angle);
        i++;
    }
    int maxStep;
    int vertex;
    double t;
    i = 0;
    while ((*vertDepth)[1][i] < (*vertDepth)[1][firstPentagon])
        i++;
    start = i;
    begin = 0;
    i = start;
    beginX = bond_length * 1.2;
    while (i < nrVertices)
    {
        for (int n : (*currentGraph)[(*vertDepth)[0][i]])
        {
            if (n != 0 && getDistance((*vertDepth)[0][i], n - 1) > vertexDistanceInHexagon)
            {
                moveTowards((*vertDepth)[0][i], n - 1, beginX);
            }
        }
        i++;
    }
    i = start;
    while (i < nrVertices)
    {
        for (int n : (*currentGraph)[(*vertDepth)[0][i]])
        {
            if (n != 0 && getDistance((*vertDepth)[0][i], n - 1) > vertexDistanceInHexagon)
            {
                moveTowardsEachother((*vertDepth)[0][i], n - 1, bond_length);
            }
        }
        i++;
    }
    t = 0.1;
    maxStep = (nrVertices - firstPentagon) * 50;
    for (int step = 0; step < maxStep; step++)
    {
        vertex = firstPentagon + (step % (nrVertices - firstPentagon));
        if (step % (nrVertices - firstPentagon) == 0)
        {
            t = (1.0 - step / maxStep);
            t = 0.5 * t * t;
        }
        findLocalOptimum((*vertDepth)[0][vertex], t, factors[0], 0, false);
    }
    maxStep = (nrVertices - firstPentagon) * 150;
    for (int step = 0; step < maxStep; step++)
    {
        vertex = firstPentagon + (step % (nrVertices - firstPentagon));
        if (step % (nrVertices - firstPentagon) == 0)
        {
            t = (1.0 - step / maxStep);
            t = 0.1 * t;
        }
        findLocalOptimum((*vertDepth)[0][vertex], t, factors[1], 0.1, false);
    }
    maxStep = (nrVertices)*250;
    for (int step = 0; step < maxStep; step++)
    {
        vertex = step % nrVertices;
        t = (1.0 - step / maxStep);
        t = 0.1 * t * t * t;
        findLocalOptimum((*vertDepth)[0][vertex], t, factors[2], 0, false);
    }
}
void NanotubeEmbedder::findLocalOptimum(int vertex, double t, double factor, double forceConvex, bool quadratic)
{
    double x = 0.0, y = 0.0, z = 0.0, d;
    for (int n : (*currentGraph)[vertex])
    {
        if (n != 0)
        {
            d = getDistance(vertex, n - 1);
            if (d > epsilon)
            {
                if (!quadratic)
                    d = (bond_length - d) / d;
                else if (bond_length - d < 0)
                    d = -pow(bond_length - d, 2) / d;
                else
                    d = pow(bond_length - d, 2) / d;
                x += d * ((*graphCoords)[vertex][0] - (*graphCoords)[n - 1][0]);
                y += d * ((*graphCoords)[vertex][1] - (*graphCoords)[n - 1][1]);
                z += d * ((*graphCoords)[vertex][2] - (*graphCoords)[n - 1][2]);
                for (int nn : (*currentGraph)[n - 1])
                {
                    if (nn != 0 && nn - 1 != n)
                    {
                        d = getDistance(vertex, nn - 1);
                        if (d > epsilon)
                        {
                            if (!quadratic)
                                d = (vertexDistanceInHexagon - d) / d;
                            else if (vertexDistanceInHexagon - d < 0)
                                d = -pow(vertexDistanceInHexagon - d, 2) / d;
                            else
                                d = pow(vertexDistanceInHexagon - d, 2) / d;
                            x += factor * d * ((*graphCoords)[vertex][0] - (*graphCoords)[n - 1][0]);
                            y += factor * d * ((*graphCoords)[vertex][1] - (*graphCoords)[n - 1][1]);
                            z += factor * d * ((*graphCoords)[vertex][2] - (*graphCoords)[n - 1][2]);
                        }
                    }
                }
            }
        }
    }
    if (forceConvex > 0)
    {
        d = sqrt(pow((*graphCoords)[vertex][0], 2) + pow((*graphCoords)[vertex][1], 2));
        d *= (1 + forceConvex);
        x *= d;
        y *= d;
    }
    (*graphCoords)[vertex][0] += t * x;
    (*graphCoords)[vertex][1] += t * y;
    (*graphCoords)[vertex][2] += t * z;
}
double NanotubeEmbedder::getDistance(int from, int to)
{
    double d = 0.0, temp;
    for (int i = 0; i < 3; i++)
    {
        temp = (*graphCoords)[from][i] - (*graphCoords)[to][i];
        d += temp * temp;
    }
    return sqrt(d);
}
void NanotubeEmbedder::calculateBoundaryParameters()
{
    int i = 0, temp = 0, start = -1;
    while ((*vertDepth)[1][i] == 0)
    {
        temp = (i + 1 >= nrVertices || (*vertDepth)[1][i + 1] != 0) ? (*vertDepth)[0][0] : (*vertDepth)[0][i + 1];
        if (areNeighbours((*vertDepth)[0][i], temp + 1))
            start = i;
        i++;
    }
    int length = i;
    i = start;
    if (start == -1)
    {
        b1 = length;
        b2 = 0;
    }
    else
    {
        if ((*vertDepth)[1][i + 1] == 1)
            i = 0;
        else
            i++;
        temp = (i + 1 >= nrVertices || (*vertDepth)[1][i + 1] != 0) ? (*vertDepth)[0][0] : (*vertDepth)[0][i + 1];
        while (haveCommonNeighbour((*vertDepth)[0][i], temp))
        {
            if ((*vertDepth)[1][i + 1] == 1)
                i = 0;
            else
                i++;
            temp = (i + 1 >= nrVertices || (*vertDepth)[1][i + 1] != 0) ? (*vertDepth)[0][0] : (*vertDepth)[0][i + 1];
        }
        if (start > i)
        {
            b2 = length - start + i;
        }
        else
            b2 = i - start + 1;
        start = i;
        while (!areNeighbours((*vertDepth)[0][i], temp + 1))
        {
            if ((*vertDepth)[1][i + 1] == 1)
                i = 0;
            else
                i++;
            temp = (i + 1 >= nrVertices || (*vertDepth)[1][i + 1] != 0) ? (*vertDepth)[0][0] : (*vertDepth)[0][i + 1];
        }
        if (start > i)
        {
            b1 = length - start + i;
        }
        else
            b1 = i - start;
    }
    if (b1 < b2)
    {
        temp = b2;
        b2 = b1;
        b1 = b2;
    }
}
bool NanotubeEmbedder::areNeighbours(int vertex, int neighbour)
{
    for (int n : (*currentGraph)[vertex])
        if (n == neighbour)
            return true;
    return false;
}
bool NanotubeEmbedder::haveCommonNeighbour(int v1, int v2)
{
    for (int n : (*currentGraph)[v1])
    {
        for (int j : (*currentGraph)[v2])
            if (n != 0 && n == j)
                return true;
    }
    return false;
}
int NanotubeEmbedder::getLengthBetween(int v1, int v2)
{
    vector<int> list;
    vector<int> notSeen(nrVertices);
    vector<int> length(nrVertices);
    for (int n : (*currentGraph)[v1])
    {
        if (n == 0)
            continue;
        if (n - 1 == v2)
            return 0;
        list.push_back(n - 1);
    }
    notSeen[v1] = 1;
    int i = 0;
    while (i < list.size())
    {
        for (int n : (*currentGraph)[list[i]])
        {
            if (n == 0)
                continue;
            if (n - 1 == v2)
                return length[list[i]] + 1;
            if (notSeen[n - 1] == 0)
            {
                list.push_back(n - 1);
                length[n - 1] = length[list[i]] + 1;
            }
        }
        notSeen[list[i]] = 1;
        i++;
    }
    return -1;
}
void NanotubeEmbedder::moveTowards(int v1, int v2, double d)
{
    double distance = getDistance(v1, v2);
    distance = (-d) / distance;
    for (int i = 0; i < 3; i++)
        (*graphCoords)[v2][i] = (*graphCoords)[v1][i] + ((*graphCoords)[v1][i] - (*graphCoords)[v2][i]) * distance;
}
void NanotubeEmbedder::moveTowardsEachother(int v1, int v2, double d)
{
    double distance = getDistance(v1, v2);
    distance = (d - distance) / distance;
    for (int i = 0; i < 3; i++)
        (*graphCoords)[v2][i] = (*graphCoords)[v1][i] + ((*graphCoords)[v1][i] - (*graphCoords)[v2][i]) * distance;
}
