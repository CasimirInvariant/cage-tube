
int intersect(EDGE *mark, int dist_to_convex)
{
   EDGE *run;
   int i;
   RESETMARKS_E;
   run = mark;
   MARK_E(run);
   for (i = 0; i < dist_to_convex; i++)
   {
      run = run->invers->next;
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      MARK_E(run);
      run = run->invers->prev;
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      MARK_E(run);
   }
   for (i = 0; i < l; i++)
   {
      run = run->invers->prev;
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      MARK_E(run);
      run = run->invers->next;
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      MARK_E(run);
   }
   for (i = 0; i < (m - dist_to_convex - 1); i++)
   {
      run = run->invers->next;
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      MARK_E(run);
      run = run->invers->prev;
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      MARK_E(run);
   }
   return 0;
}
int intersect_v(EDGE *mark, int dist_to_convex)
{
   EDGE *run;
   int i;
   RESETMARKS_E;
   run = mark;
   MARK_E(run);
   for (i = 0; i < dist_to_convex; i++)
   {
      run = run->invers->prev;
      MARK_E(run);
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      run = run->invers->next;
      MARK_E(run);
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
   }
   for (i = 0; i < l; i++)
   {
      run = run->invers->next;
      MARK_E(run);
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      run = run->invers->prev;
      MARK_E(run);
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
   }
   for (i = 0; i < (m - dist_to_convex - 1); i++)
   {
      run = run->invers->prev;
      MARK_E(run);
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
      run = run->invers->next;
      MARK_E(run);
      if (ISMARKED_E(run->invers))
      {
         return 1;
      }
   }
   return 0;
}
int glue(EDGE *mark)
{
   EDGE *edge1, *edge2;
   int i;
   edge1 = mark;
   edge2 = isomorphism_tube;
   for (i = 1; i < l; i++)
   {
      edge1->end = edge2->start;
      edge1->invers = edge2;
      edge2->end = edge1->start;
      edge2->invers = edge1;
      edge1 = edge1->next->invers->next->invers->next;
      edge2 = edge2->prev->invers->prev->invers->prev;
   }
   edge1->end = edge2->start;
   edge1->invers = edge2;
   edge2->end = edge1->start;
   edge2->invers = edge1;
   edge1 = edge1->next->invers->next->invers->next->invers->next;
   edge2 = edge2->prev->invers->prev;
   for (i = 1; i < m; i++)
   {
      edge1->end = edge2->start;
      edge1->invers = edge2;
      edge2->end = edge1->start;
      edge2->invers = edge1;
      edge1 = edge1->next->invers->next->invers->next;
      edge2 = edge2->prev->invers->prev->invers->prev;
   }
   edge1->end = edge2->start;
   edge1->invers = edge2;
   edge2->end = edge1->start;
   edge2->invers = edge1;
   return 1;
}
int unglue2(EDGE *mark)
{
   EDGE *edge1, *edge2;
   int i;
   edge1 = mark;
   edge2 = isomorphism_tube;
   for (i = 1; i < l; i++)
   {
      edge1->end = outside;
      edge1->invers = NULL;
      edge2->end = outside;
      edge2->invers = NULL;
      edge1 = edge1->next->invers->next->invers->next;
      edge2 = edge2->prev->invers->prev->invers->prev;
   }
   edge1->end = outside;
   edge1->invers = NULL;
   edge2->end = outside;
   edge2->invers = NULL;
   edge1 = edge1->next->invers->next->invers->next->invers->next;
   edge2 = edge2->prev->invers->prev;
   for (i = 1; i < m; i++)
   {
      edge1->end = outside;
      edge1->invers = NULL;
      edge2->end = outside;
      edge2->invers = NULL;
      edge1 = edge1->next->invers->next->invers->next;
      edge2 = edge2->prev->invers->prev->invers->prev;
   }
   edge1->end = outside;
   edge1->invers = NULL;
   edge2->end = outside;
   edge2->invers = NULL;
   return 1;
}
int unglue(EDGE *mark, int dist_to_convex)
{
   EDGE *run;
   int i;
   run = mark;
   for (i = 0; i < dist_to_convex; i++)
   {
      run = run->invers->next->invers->next;
      run->end = outside;
      run = run->next;
   }
   for (i = 0; i < l; i++)
   {
      run = run->invers->next;
      run->end = outside;
      run = run->next->invers->next;
   }
   for (i = 0; i < (m - dist_to_convex); i++)
   {
      run = run->invers->next->invers->next;
      run->end = outside;
      run = run->next;
   }
   return 1;
}
int ungluev(EDGE *mark, int dist_to_convex)
{
   EDGE *run;
   int i;
   run = mark;
   for (i = 0; i < dist_to_convex; i++)
   {
      run = run->invers->prev->invers->prev;
      run->end = outside;
      run = run->prev;
   }
   for (i = 0; i < l; i++)
   {
      run = run->invers->prev;
      run->end = outside;
      run = run->prev->invers->prev;
   }
   for (i = 0; i < (m - dist_to_convex); i++)
   {
      run = run->invers->prev->invers->prev;
      run->end = outside;
      run = run->prev;
   }
   return 1;
}
void reglue(EDGE *mark, int dist_to_convex)
{
   EDGE *edge;
   int i;
   edge = mark;
   for (i = 0; i < dist_to_convex; i++)
   {
      edge = edge->invers->next->invers->next;
      edge->end = edge->invers->start;
      edge = edge->next;
   }
   for (i = 0; i < l; i++)
   {
      edge = edge->invers->next;
      edge->end = edge->invers->start;
      edge = edge->next->invers->next;
   }
   for (i = 0; i < (m - dist_to_convex); i++)
   {
      edge = edge->invers->next->invers->next;
      edge->end = edge->invers->start;
      edge = edge->next;
   }
}
void regluev(EDGE *mark, int dist_to_convex)
{
   EDGE *edge;
   int i;
   edge = mark;
   for (i = 0; i < dist_to_convex; i++)
   {
      edge = edge->invers->prev->invers->prev;
      edge->end = edge->invers->start;
      edge = edge->prev;
   }
   for (i = 0; i < l; i++)
   {
      edge = edge->invers->prev;
      edge->end = edge->invers->start;
      edge = edge->prev->invers->prev;
   }
   for (i = 0; i < (m - dist_to_convex); i++)
   {
      edge = edge->invers->prev->invers->prev;
      edge->end = edge->invers->start;
      edge = edge->prev;
   }
}
int lm_patch_is_canonical(EDGE *mark)
{
   int i, j, k, result, dist_to_convex = 0, pentagon_found, auts = 0, mirror = 0, eight;
   int dist_to_convex2 = 0, zero;
   EDGE *run, *starting_point, *original_pentagon_edge;
   static VERTEX *representation = NULL;
   if (representation == NULL)
   {
      if ((representation = (VERTEX *)calloc(maxlabel * 2, sizeof(VERTEX))) == NULL)
      {
         fprintf(stderr, "Cannot allocate memory for the representation.\n");
         exit(0);
      }
   }
   zero = 1;
   run = mark;
   run = run->prev;
   while (run->invers->pentagon_right == 0)
   {
      if (dist_to_convex == (m - 1))
      {
         return 0;
      }
      run = run->invers->next->invers->prev;
      dist_to_convex++;
   }
   if (mirror > 0)
   {
      (auts_statistic_mirror[mirror])++;
   }
   else
   {
      (auts_statistic[auts])++;
   }
   if (dist_to_convex == 0)
   {
      if (l != m)
      {
         auts_statistic[1]++;
         if (symmetry == 1 || symmetry == 0)
         {
            return 1;
         }
         return 0;
      }
      else
      {
         get_representation(mark, representation);
         starting_point = mark->prev->invers->prev;
         result = check_representation(starting_point, representation, 0);
         if (result == 0)
         {
            auts_statistic[1]++;
            if (symmetry == 1 || symmetry == 0)
            {
               return 1;
            }
            return 0;
         }
         if (result == 1)
         {
            auts_statistic_mirror[1]++;
            if (symmetry == -1 || symmetry == 0)
            {
               return 1;
            }
            return 0;
         }
         if (result == 1)
         {
            return 0;
         }
      }
   }
   original_pentagon_edge = run->invers;
   run = mark;
   run = run->prev->invers;
   while (run->pentagon_right == 0)
   {
      if (dist_to_convex2 == (l - 1))
      {
         return 0;
      }
      run = run->invers->prev->invers->next;
      dist_to_convex2++;
   }
   if (l == m)
   {
      if (dist_to_convex2 < dist_to_convex)
      {
         return 0;
      }
   }
   get_representation(mark, representation);
   glue(mark);
   for (i = 0; i < pentagonedges; i++)
   {
      for (j = 0; j < dist_to_convex; j++)
      {
         run = pentagons[i];
         pentagon_found = 0;
         if (run->invers->pentagon_right == 1)
         {
            goto one;
         }
         for (k = 0; k < j; k++)
         {
            run = run->invers->next->invers->prev;
            if (run->invers->pentagon_right == 1)
            {
               goto one;
            }
         }
         for (k = 0; k < l; k++)
         {
            if (run->pentagon_right == 1)
            {
               pentagon_found = 1;
            }
            run = run->invers->prev->invers->next;
            if (run->invers->pentagon_right == 1)
            {
               goto one;
            }
         }
         for (k = 0; k < (m - j); k++)
         {
            run = run->invers->next->invers->prev;
            if (run->invers->pentagon_right == 1)
            {
               goto one;
            }
         }
         if (run == pentagons[i] && pentagon_found == 1)
         {
            eight = intersect(run, j);
            if (eight == 0)
            {
               unglue2(mark);
               return 0;
            }
         }
      one:
         if (l == m)
         {
            run = pentagons[i];
            pentagon_found = 0;
            run = run->invers;
            if (run->pentagon_right == 1)
            {
               goto two;
            }
            for (k = 0; k < j; k++)
            {
               run = run->invers->prev->invers->next;
               if (run->pentagon_right == 1)
               {
                  goto two;
               }
            }
            for (k = 0; k < l; k++)
            {
               if (run->invers->pentagon_right == 1)
               {
                  pentagon_found = 1;
               }
               run = run->invers->next->invers->prev;
               if (run->pentagon_right == 1)
               {
                  goto two;
               }
            }
            for (k = 0; k < (m - j); k++)
            {
               run = run->invers->prev->invers->next;
               if (run->pentagon_right == 1)
               {
                  goto two;
               }
            }
            run = run->invers;
            if (run == pentagons[i] && pentagon_found == 1)
            {
               run = run->invers;
               eight = intersect_v(run, j);
               if (eight == 0)
               {
                  unglue2(mark);
                  return 0;
               }
            }
         }
      two:
         continue;
      }
      run = pentagons[i];
      if (run == original_pentagon_edge)
      {
         auts++;
         goto three;
      }
      pentagon_found = 0;
      if (run->invers->pentagon_right == 1)
      {
         goto three;
      }
      for (j = 0; j < dist_to_convex; j++)
      {
         run = run->invers->next->invers->prev;
         if (run->invers->pentagon_right == 1)
         {
            goto three;
         }
      }
      starting_point = run;
      starting_point = starting_point->invers->next;
      for (j = 0; j < l; j++)
      {
         if (run->pentagon_right == 1)
         {
            pentagon_found = 1;
         }
         run = run->invers->prev->invers->next;
         if (run->invers->pentagon_right == 1)
         {
            goto three;
         }
      }
      for (j = 0; j < (m - dist_to_convex); j++)
      {
         run = run->invers->next->invers->prev;
         if (run->invers->pentagon_right == 1)
         {
            goto three;
         }
      }
      if (run == pentagons[i] && pentagon_found == 1)
      {
         eight = intersect(run, dist_to_convex);
         if (eight == 0)
         {
            if (zero == 1)
            {
               unglue2(mark);
               get_representation(mark, representation);
               glue(mark);
               unglue(run, dist_to_convex);
               result = check_representation(starting_point, representation, 1);
               if (result == 2)
               {
                  reglue(run, dist_to_convex);
                  unglue2(mark);
                  return 0;
               }
               else if (result == 1)
               {
                  auts++;
               }
               zero = 0;
            }
            else
            {
               unglue(run, dist_to_convex);
               result = check_representation(starting_point, representation, 1);
               if (result == 2)
               {
                  reglue(run, dist_to_convex);
                  unglue2(mark);
                  return 0;
               }
               else if (result == 1)
               {
                  auts++;
               }
            }
            reglue(run, dist_to_convex);
         }
      }
   three:
      if (l == m)
      {
         run = pentagons[i];
         pentagon_found = 0;
         run = run->invers;
         if (run->pentagon_right == 1)
         {
            goto four;
         }
         for (j = 0; j < dist_to_convex; j++)
         {
            run = run->invers->prev->invers->next;
            if (run->pentagon_right == 1)
            {
               goto four;
            }
         }
         starting_point = run;
         starting_point = starting_point->invers->prev;
         for (j = 0; j < l; j++)
         {
            if (run->invers->pentagon_right == 1)
            {
               pentagon_found = 1;
            }
            run = run->invers->next->invers->prev;
            if (run->pentagon_right == 1)
            {
               goto four;
            }
         }
         for (j = 0; j < (m - dist_to_convex); j++)
         {
            run = run->invers->prev->invers->next;
            if (run->pentagon_right == 1)
            {
               goto four;
            }
         }
         run = run->invers;
         if (run == pentagons[i] && pentagon_found == 1)
         {
            run = run->invers;
            eight = intersect_v(run, dist_to_convex);
            if (eight == 0)
            {
               if (zero == 1)
               {
                  unglue2(mark);
                  get_representation(mark, representation);
                  glue(mark);
                  ungluev(run, dist_to_convex);
                  result = check_representation(starting_point, representation, 0);
                  if (result == 2)
                  {
                     regluev(run, dist_to_convex);
                     unglue2(mark);
                     return 0;
                  }
                  else if (result == 1)
                  {
                     mirror++;
                  }
                  zero = 0;
               }
               else
               {
                  ungluev(run, dist_to_convex);
                  result = check_representation(starting_point, representation, 0);
                  if (result == 2)
                  {
                     regluev(run, dist_to_convex);
                     unglue2(mark);
                     return 0;
                  }
                  else if (result == 1)
                  {
                     mirror++;
                  }
               }
               regluev(run, dist_to_convex);
            }
         }
      }
   four:
      continue;
   }
   unglue2(mark);
   if (mirror > 0)
   {
      (auts_statistic_mirror[mirror])++;
   }
   else
   {
      (auts_statistic[auts])++;
   }
   if (mirror && -auts == symmetry || auts == symmetry || symmetry == 0)
   {
      return 1;
   }
   return 0;
}
